<?php
include ('header.php');
include ('sidebar.php');

?>
    <div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo $custdir; ?>/acp/">Dashboard</a>
            </li>

        </ol>
        <div class="card mb-3">
            <div class="card-header">
                <i class="fad fa-box-heart"></i> Store Items!</div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover table-dark" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Item ID</th>
                            <th>Category</th>
                            <th>Item Name</th>
                            <th>Price</th>
                            <th>Tools</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Item ID</th>
                            <th>Category</th>
                            <th>Item Name</th>
                            <th>Price</th>
                            <th>Tools</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        <?php
                        $item_query = $mysqliA->query("SELECT * FROM `store_items`") or die (mysqli_error($mysqliA));
                        $num_query = $item_query->num_rows;
                        if($num_query < 1)
                        {
                            echo '<tr><td colspan="6">There are no items!</td></tr>';
                        }
                        else
                        {
                            while($res = $item_query->fetch_assoc())
                            {
                                $storeID = $res['id'];
                                $itemID = $res['item_id'];
                                $item_name = $res['item_name'];
                                $itemcatid = $res['category'];
                                $pricedb = $res['price'];

                                // let´s query store category by itemcatid
                                $category_query = $mysqliA->query("SELECT * FROM `store_items_categorys` WHERE id = $itemcatid;") or die (mysqli_error($mysqliA));
                                while($res_category = $category_query->fetch_assoc())
                                {
                                    $category = $res_category['name'];
                                }

                                    if($pricedb == 0)
                                    {
                                        $price = "Free";
                                    }

                                    if($pricedb > 0)
                                    {
                                        $price = $pricedb;
                                    }


                                echo '
                                    <tr>
                                       <td>'. $storeID .'</td>
                                       <td>'. $itemID .'</td>
                                       <td>'.$category.'</td>
                                       <td><a href="#" data-wowhead="item='. $itemID .'">'. $item_name .'</a></td>
                                       <td><span class="badge badge-warning">'. $price .' <i class="fad fa-coin"></i></span></td>
                                       <td><a href="'.$custdir.'/acp/item-edit.php?id='. $storeID .'" class="btn btn-sm btn-primary"><i class="fad fa-edit"></i> Edit</a> <a href="'.$custdir.'/acp/item-delete.php?id='. $storeID .'" class="btn btn-sm btn-danger" onclick="return confirm(\'Are you sure?\')"><i class="fad fa-trash"></i> Delete</a></td>
                                    </tr>
                                    ';
                            }
                        }
                        ?>

                        </tbody>
                    </table>
                    <a href="<?php echo $custdir; ?>/acp/item-add.php" class="btn btn-success"><i class="fad fa-plus-circle"></i> Add new item</a>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
<?php
include ('footer.php');
?>