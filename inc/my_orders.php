<?php
$sessid = $_SESSION['id'];

//let's do more stuff
$get_acc_id = $mysqliA->query("SELECT `id` FROM `account` WHERE `battlenet_account` = '$sessid';") or die (mysqli_errno($mysqliA));
while ($acc = $get_acc_id->fetch_assoc()) {
    $acc_id = $acc['id'];
}

//lets get orders
$my_orders = $mysqliA->query("SELECT * FROM `payments` WHERE `account_id` = '$acc_id';") or die (mysqli_errno($mysqliA));
$num_orders = $my_orders->num_rows;
if($num_orders < 1)
{
    echo '
        <h5>Your orders!</h5>
        <div class="alert alert-warning">
            <i class="fad fa-exclamation-triangle"></i> You don\'t have any orders!
        </div>
    ';
}
else
{
    echo '
        <h5>Your orders</h5>
        <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">Order ID</th>
            <th scope="col">Price</th>
            <th scope="col">Payment status</th>
            <th scope="col">Coins status</th>
        </tr>
        </thead>
        <tbody>
        ';
    while($res = $my_orders->fetch_assoc())
    {
        $order_id = $res['id'];
        $payment_id = $res['payment_id'];
        $price = $res['amount'];
        $currency = $res['currency'];
        $order_status = $res['payment_status'];
        $coins_claimed = $res['coins_claimed'];


        //forgot to make the claim available only if the status of the oder is success
        if($order_status === 'approved')
        {
            switch ($coins_claimed) {
                case "0":
                    $link = '<a href="/claim-coins.php?id='. $order_id .'" class="btn btn-sm btn-outline-warning"><i class="fad fa-coin"></i> Claim coins</a>';
                    break;
                case "1":
                    $link = '<span class="badge badge-success">Coins claimed!</span>';
                    break;
            }
        }
        else
        {
            $link = '<span class="badge badge-warning">Cannot be claimed!</span>';
        }

        switch ($order_status) {
            case "approved":
                $class = 'success';
                break;
            case "failed":
                $class = 'warning';
                break;
            case "created":
                $class = 'primary';
                break;
        }

        switch ($order_status) {
            case "approved":
                $status = 'Approved';
                break;
            case "failed":
                $status = 'Failed';
                break;
            case "created":
                $status = 'Created';
                break;
        }
        echo '
        <tr>
        <td>'. $payment_id .'</td>
        <td>'. $price .' '. $currency .'</td>
        <td><span class="badge badge-'. $class .'">'. $status .'</span></td>
        <td>'. $link .'</td>
        </tr>
        ';
    }
    echo '</table>';
}
